meta:
  id: aztechsat1
  endian: be
doc: |
  :field csp_h: azt.csp_h
  :field url: azt.url
  :field start_k: azt.start_k
  :field vbooster_1: azt.eps.vbooster_1
  :field vbooster_2: azt.eps.vbooster_2
  :field vbooster_3: azt.eps.vbooster_3
  :field vbatt: azt.eps.vbatt
  :field curr_in_1: azt.eps.curr_in_1
  :field curr_in_2: azt.eps.curr_in_2
  :field curr_in_3: azt.eps.curr_in_3
  :field currsun: azt.eps.currsun
  :field currsys: azt.eps.currsys
  :field curr_out_1: azt.eps.curr_out_1
  :field curr_out_2: azt.eps.curr_out_2
  :field curr_out_3: azt.eps.curr_out_3
  :field curr_out_4: azt.eps.curr_out_4
  :field output_1_status: azt.eps.output_1_status
  :field output_2_status: azt.eps.output_2_status
  :field output_3_status: azt.eps.output_3_status
  :field output_4_status: azt.eps.output_4_status
  :field wdt_i2c_time_left: azt.eps.wdt_i2c_time_left
  :field wdt_gnd_time_left: azt.eps.wdt_gnd_time_left
  :field counter_wdt_gnd: azt.eps.counter_wdt_gnd
  :field temp: azt.eps.temp
  :field temp_pa: azt.radio.temp_pa
  :field last_rssi: azt.radio.last_rssi
  :field bgnd_rssi: azt.radio.bgnd_rssi
  :field obc_temp_pa: azt.obc.temp_pa
  :field pwm_current: azt.obc.pwm_current
  :field boot_cause: azt.obc.boot_cause
  :field magneto_x: azt.obc.magneto_x
  :field magneto_y: azt.obc.magneto_y
  :field magneto_z: azt.obc.magneto_z
  :field gyro_x: azt.obc.gyro_x
  :field gyro_y: azt.obc.gyro_y
  :field gyro_z: azt.obc.gyro_z
  :field n_channel: azt.payload.n_channel
  :field n_burst: azt.payload.n_burst
  :field min_interval: azt.payload.min_interval
  :field max_interval: azt.payload.max_interval
  :field status: azt.payload.status
  :field seconds_since_last_tx: azt.payload.seconds_since_last_tx
  :field seconds_until_next_tx: azt.payload.seconds_until_next_tx
  :field packet_size: azt.payload.packet_size
  :field burst_number: azt.payload.burst_number
  :field seconds_until_burst_2: azt.payload.seconds_until_burst_2
  :field seconds_until_burst_3: azt.payload.seconds_until_burst_3
  :field total_tx_messages_current_mode: azt.payload.total_tx_messages_current_mode
  :field total_tx_packet_since_power_on: azt.payload.total_tx_packet_since_power_on
  :field stinger_antenna_temp: azt.payload.stinger_antenna_temp
  :field lm70_temp: azt.payload.lm70_temp
  :field packet_id: azt.packet_id
  :field coarse_sun_sensors_1: azt.adcs.coarse_sun_sensors_1
  :field coarse_sun_sensors_2: azt.adcs.coarse_sun_sensors_2
  :field coarse_sun_sensors_3: azt.adcs.coarse_sun_sensors_3
  :field coarse_sun_sensors_4: azt.adcs.coarse_sun_sensors_4
  :field coarse_sun_sensors_5: azt.adcs.coarse_sun_sensors_5
  :field panel_y_pos_temp: azt.adcs.panel_y_pos_temp
  :field panel_x_pos_temp: azt.adcs.panel_x_pos_temp
  :field panel_x_neg_temp: azt.adcs.panel_x_neg_temp
  :field panel_y_neg_temp: azt.adcs.panel_y_neg_temp
  :field panel_z_neg_temp: azt.adcs.panel_z_neg_temp
  :field status_bdot: azt.adcs.status_bdot
  :field bdot_rate_slow: azt.adcs.bdot_rate_slow
  :field bdot_rate_slow_2: azt.adcs.bdot_rate_slow_2
  :field bdot_detumb: azt.adcs.bdot_detumb
  :field end_k: azt.end_k
  

seq:
  - id: azt
    type: aztechsat
    doc-ref: 'https://github.com/miligon/aztechsat-decoder-python'
 
  
types:  

  aztechsat:
    seq:
    - id: csp_h
      type: u4
    - id: url
      type: str
      encoding: ASCII
      size: 27
    - id: start_k
      type: str
      encoding: ASCII
      size: 4 
    - id: eps
      type: eps_hk
    - id: radio
      type: radio_hk
    - id: obc
      type: obc_hk
    - id: payload
      type: pyl_hk
    - id: packet_id
      type: u4
    - id: adcs
      type: adcs_hk
    - id: end_k
      type: str
      encoding: ASCII
      size: 5

  eps_hk:
    seq:
    - id: vbooster_1
      type: u2
    - id: vbooster_2
      type: u2
    - id: vbooster_3
      type: u2
    - id: vbatt
      type: u2
    - id: curr_in_1
      type: u2
    - id: curr_in_2
      type: u2
    - id: curr_in_3
      type: u2
    - id: currsun
      type: u2
    - id: currsys
      type: u2
    - id: curr_out_1
      type: u2
    - id: curr_out_2
      type: u2
    - id: curr_out_3
      type: u2
    - id: curr_out_4
      type: u2
    - id: output_1_status
      type: u1
    - id: output_2_status
      type: u1
    - id: output_3_status
      type: u1
    - id: output_4_status
      type: u1
    - id: wdt_i2c_time_left
      type: u4
    - id: wdt_gnd_time_left
      type: u4
    - id: counter_wdt_gnd
      type: u4
    - id: temp
      type: s2
  
  radio_hk:
    seq:
    - id: temp_pa
      type: s2
    - id: last_rssi
      type: s2
    - id: bgnd_rssi
      type: s2
  
  obc_hk:
    seq:
    - id: temp_pa
      type: s2
    - id: pwm_current
      type: u2
    - id: boot_cause
      type: u4
    - id: magneto_x
      type: f4
    - id: magneto_y
      type: f4
    - id: magneto_z
      type: f4
    - id: gyro_x
      type: f4
    - id: gyro_y
      type: f4
    - id: gyro_z
      type: f4
    
  pyl_hk:
    seq:
      - id: n_channel
        type: u1
      - id: n_burst
        type: u1
      - id: min_interval
        type: u1
      - id: max_interval
        type: u1
      - id: status
        type: u1
      - id: seconds_since_last_tx
        type: u2
      - id: seconds_until_next_tx
        type: u2
      - id: packet_size
        type: u1
      - id: burst_number
        type: u1
      - id: seconds_until_burst_2
        type: u2
      - id: seconds_until_burst_3
        type: u2
      - id: total_tx_messages_current_mode
        type: u2
      - id: total_tx_packet_since_power_on
        type: u2
      - id: stinger_antenna_temp
        type: u1
      - id: lm70_temp
        type: u2
        
  adcs_hk:
    seq:
      - id: coarse_sun_sensors_1
        type: u2
      - id: coarse_sun_sensors_2
        type: u2
      - id: coarse_sun_sensors_3
        type: u2
      - id: coarse_sun_sensors_4
        type: u2
      - id: coarse_sun_sensors_5
        type: u2
      - id: panel_y_pos_temp
        type: f4
      - id: panel_x_pos_temp
        type: f4
      - id: panel_x_neg_temp
        type: f4
      - id: panel_y_neg_temp
        type: f4
      - id: panel_z_neg_temp
        type: f4
      - id: status_bdot
        type: u1
      - id: bdot_rate_slow
        type: f4
      - id: bdot_rate_slow_2
        type: f4
      - id: bdot_detumb
        type: u1